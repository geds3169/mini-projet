FROM nginx:1.23.1
LABEL maintainer="Guilhem SCHLOSSER"
RUN apt-get update && \
    apt-get upgrade -y && \
     apt-get install apt-utils -y && \
    apt-get install -y curl git zip unzip tree
RUN rm -Rf /usr/share/nginx/html/*
RUN git clone https://github.com/diranetafen/static-website-example.git /usr/share/nginx/html
ADD https://gitlab.com/geds3169/mini-projet/-/raw/main/nginx.conf /etc/nginx/conf.d/default.conf
CMD sed -i -e 's/$PORT/'"$PORT"'/g' /etc/nginx/conf.d/default.conf && nginx -g 'daemon off;'
